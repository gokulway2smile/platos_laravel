<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Base;
// use App\Http\Controllers\restrictcontroller;

use App\Models\AdminModel;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Admin extends Controller
{
    
    public function Auth_weblogin(Request $request)
    {
 
        
         $password = $request->input('password');
        
        $username = $request->input('username');

        $data =  AdminModel::where('is_active','true')->where('adm_username',$username)->where('adm_password',$password)->count();
         
        if($data>0)
        {

            $data = AdminModel::where('adm_username', $username)->where('adm_password', $password)->get()->toArray();
            
            $key = Base::token($data[0]['adm_id'], AdminModel::class,false,false,'Admin');
            $data[0]['token']= $key;
            return Base::touser($data, true);
        }
        elseif($data==0)
        {
            return Base::touser("Invalid username or password");
        }
        else
        {
            return Base::throwerror();
        }        
    }

        
        public function store(Request $request)
    {
        // 
        $rules = [
            'role_id'    => 'required',
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            // 'street'        => 'required',
            // 'city'          => 'required',
            // 'state'         => 'required',
            //'zipcode'       => 'required',
            // 'country'       => 'required',
            // 'profile_image' => 'required',
            'email'      => 'required|email|unique:user',
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        
    }


   

}
