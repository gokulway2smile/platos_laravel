<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\AmenitiesModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Amenities extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = AmenitiesModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = AmenitiesModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    

    public function store(Request $request)
    {
        // 
        $rules = [
            'amen_title'    => 'required'               
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $amenities                      = new AmenitiesModel();
        $amenities->amen_title       = $data['amen_title'];
        $amenities->amen_description      = $data['amen_description']; 
        $amenities->save();

        return Base::touser('Amenities Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }


    public function show(Request $request,$id)
    {
            

            $data = AmenitiesModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'amen_title'    => 'required'     
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $amenities                      = new AmenitiesModel();
        $amenities                      = $amenities->where('amen_id', '=', $id)->first();
        $amenities->amen_title          = $data['amen_title'];
        $amenities->amen_description    = $data['amen_description']; 
        $amenities->save();
        return Base::touser('Amenities Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new AmenitiesModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Amenities Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Amenities its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
