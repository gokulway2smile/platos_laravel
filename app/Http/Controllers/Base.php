<?php
namespace App\Http\Controllers;

use App\Models\Auth_token;
use App\Models\WebadmAuthModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Firebase\JWT\JWT;
use Vinkla\Hashids\Facades\Hashids;

use Carbon\Carbon;
use \DateTime;
use \DateTimeZone;

class Base extends Controller
{

    const salt = "4Ud0ZxvydK0pc+oBSsJ11ResjdcGJTVBGPHgtO1+SBc=";

    public static function touser($data, $status = false,$code = 200)
    {
        $st = 'error';

        if ($status) {
            $st = 'ok';
        }

        /* $else = json_encode(array('data' => $data, 'status' => $st));



         $data = $else;
         */

         $data = json_encode(array('data' => $data, 'status' => $st));

        return response($data, $code)->withHeaders(['Content-Type' => 'application/json']);
    }

    public static  function ConvertTimezone($value,$get = false)
    {
        if($get)
        {
            if($value == null)
            {
               return $value;
            }
            else
            {
                $value = Carbon::createFromTimestamp(strtotime($value))->timezone(self::client_timezone());
             return $value->toDateTimeString();
            }
        }
        if ($value instanceof Carbon) {

            return $value;

        }

     return Carbon::createFromTimestamp(strtotime($value))->timezone(self::client_timezone());
    }

    public static function client_timezone()
        {
         return "UTC";
        }


        public static function Token_fromID($id,$is_mobile=false)
     {         
          $get_userid = Auth_token::where('jwt_token', '=', $id)
          // ->where('is_mobilelogin',$is_mobile)
          ->orderBy('auth_id', 'DESC')->first();

           if($get_userid)
              {
                    return $get_userid['catt_id'];
              }
              else {
                return false;
            }
     }
        public static function Tokenfrom_SubcaterID($id,$is_mobile=false)
        {

             $get_userid = Auth_token::where('jwt_token', '=', $id)
                  // ->where('is_mobilelogin',$is_mobile)
                  ->orderBy('auth_id', 'DESC')->first();

                   if($get_userid)
                      {
                            return $get_userid['sub_admin_id'];
                      }
                      else {
                        return false;
                    }


        }
             



    public static function ip()
    {
        $ip = '';

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

     public static function user_agent()
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }

    public static function get_domin()
    {
        $url = parse_url(\URL::current())['host'];

        return $url;
    }

    public static function throwerror()
    {
        return self::touser('Data or System Error');
    }


    public static function client_data()
    {
        if (array_key_exists('HTTP_X_CLIENT_DATA', $_SERVER)) {
            if (null == $_SERVER['HTTP_X_CLIENT_DATA'] && empty($_SERVER['HTTP_X_CLIENT_DATA'])) {
                self::app_unauthorized();
            }
        } else {
            self::app_unauthorized();
        }

        return $_SERVER['HTTP_X_CLIENT_DATA'];
    }


public static function token($data, $model = null,$is_mobile = false,$is_subadmin = false,$sub_admin_id)
    {
        

        $salt = self::salt;
        $user_ip = self::ip();
        $user_agent = self::user_agent();

        $token = array(
            "apitoken" => Hashids::encode($data).'_'.\Hash::make(str_random(500)),
            "iss" => self::get_domin()
            );

       $jwt = JWT::encode($token, $salt);

         $exist = count(Auth_token::where('jwt_token', '=', $jwt)->get()->toArray());
        // $session = clientinfo::firstOrNew(array('client_data' => self::client_data()));
        // $session->client_ip = $user_ip;
        // $session->client_data = self::client_data();
        // $session->client_info = $user_agent;
        // $session->save();



//     $array =
//     [
//     "auth_key" => $jwt,
//     "auth_user_agent" => $user_agent,
//     "auth_ip" => $user_ip,
//     "auth_model" => $model,
//     "auth_user_id" => $data,
//     ];



//         $api = ApiAuth::updateOrCreate($array);

// return $jwt;

         if($sub_admin_id != 'Admin')
         {
        if ($exist > 0) {
            $api = Auth_token::where('jwt_token', '=', $jwt)->first();
            $api->jwt_token = $jwt;
            $api->auth_user_agent = $user_agent;
            $api->auth_ip = $user_ip;
            $api->auth_model = $model;
            $api->is_mobilelogin = $is_mobile;
            $api->catt_id = $data;
            $api->subAdmin = $is_subadmin;
            $api->sub_admin_id = $sub_admin_id;
            $api->save();

        } else {
            $api = new Auth_token;
            $api->jwt_token = $jwt;
            $api->auth_user_agent = $user_agent;
            $api->auth_ip = $user_ip;
            $api->auth_model = $model;
            $api->is_mobilelogin = $is_mobile;
            $api->catt_id = $data;
            $api->subAdmin = $is_subadmin;
            $api->sub_admin_id = $sub_admin_id;
            $api->save();
        }

        }
        else
        {
            $exist = count(WebadmAuthModel::where('jwt_token', '=', $jwt)->get()->toArray());

            if ($exist > 0) {
            $api = WebadmAuthModel::where('jwt_token', '=', $jwt)->first();
            $api->jwt_token = $jwt;
            $api->auth_user_agent = $user_agent;
            $api->auth_ip = $user_ip;
            $api->auth_model = $model;
            $api->save();

        } else {
            $api = new WebadmAuthModel;
            $api->jwt_token = $jwt;
            $api->auth_user_agent = $user_agent;
            $api->auth_model = $model;
            $api->auth_ip = $user_ip;
            $api->save();
        }


            

        }



        return $jwt;
    }

}?>