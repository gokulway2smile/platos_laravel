<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\CatterModel;
use App\Models\Auth_token;
use App\Models\CattAddressModel;
use App\Models\CatterGroupModel;
use App\Models\CatererAuditModel;
use App\Models\CattUserModel;
use App\Models\SpecialCategoryModel;
use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Catters extends Controller
{
    
    public function Create_admin(Request $request)
    {
        $rules = [
            'FirstName'    => 'required',
            'EmailAddress' => 'required',
            'MobileNo'  => 'required',
            'Password'      => 'required',
            'DeliveryPincode'        => 'required',
            'LeadTime'          => 'required',
            'DeliveryTime'         => 'required',
            'MinOrder'       => 'required',
            'maxOrder'       => 'required',
            'EventType' => 'required',
            // 'email'      => 'required|email|unique:user',
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

       $totalcount = CatterModel::where('is_active','true')->count();
       // ->orWhere(function ($query) {
       //  $query->where('catt_email_address', $data['FirstName'])
       //        ->where('catt_first_name', $data['FirstName']);
       //  })

       // ->where(function($q) {
       //   $q->where('catt_first_name', $data['FirstName'])
       //     ->orWhere('catt_email_address', $data['EmailAddress'])
       //     ->orWhere('catt_mobile_no', $data['MobileNo']);
       //  })

       
        
        if($totalcount==0)
        {

        $caterer                        = new CatterModel();
        $caterer->catt_first_name       = $data['FirstName'];
        $caterer->catt_sur_name         = $data['SurName'];
        $caterer->catt_email_address    = $data['EmailAddress'];
        $caterer->catt_mobile_no        = $data['MobileNo'];
        $caterer->catt_password         = $data['Password'];
        $caterer->catt_image            = $data['Password'];
        $caterer->catt_delivery_pincode = $data['DeliveryPincode'];
        $caterer->catt_delivery_time    = $data['DeliveryTime'];
        $caterer->catt_min_order        = $data["MinOrder"];
        $caterer->catt_max_order        = $data['maxOrder'];
        $caterer->catt_lead_time        = $data['maxOrder'];
        $caterer->catt_event_type       = $data['EventType']? $data['EventType'] : "";

        foreach ($data['catt_files'] as $key => $value) {

            if($key == 'GST')
            {
                $caterer->catt_gst_certificate  = $value;
            }
            else
            {
                $caterer->catt_gst_certificate  = "";
            }

            if($key == 'Food')
            {
                $caterer->catt_food_certificate  = $value;
            }
            else
            {
                $caterer->catt_food_certificate  = "";
            }

            if($key == 'upload')
            {
                $caterer->catt_file_upload  = $value;
            }
            else
            {
                $caterer->catt_file_upload  = "";
            }          
        }
        
        $caterer->catt_score            = $data['Scores'] ? $data['Scores'] : "";
        $caterer->catt_category         = $data['FileUpload'];
        
        $caterer->save();

        return Base::touser('Caterer Created', true);

        }
        else
        {
            return Base::touser('Caterer Already Exists');
        }
          
    }

    
    public function caterer_webLogin(Request $request)
    {
        
        $input = $request->input('data');

        $password = $input['password'];

        $valid = CatterModel::where('catt_email_address', '=', $input['username'])
               ->first();

        $sub_caterer = CattUserModel::where('cat_usr_email', '=', $input['username'])
               ->first();

        if($valid)
        {
            if ((decrypt($valid->catt_password) == $input['password'])) {
                        
                if(isset($input['is_mob_login']))
                {
                    $key = Base::token($valid['catt_id'], CatterModel::class,$input['is_mob_login'],false,$valid['catt_id']);    
                }
                else
                {
                    $key = Base::token($valid['catt_id'], CatterModel::class,false,false,$valid['catt_id']);   
                }
                
                $valid['token']= $key;
                $valid['caterer_admin'] =true;

                if($valid['is_reseted_password'] == true)
                {
                    $valid['is_dashborad']= false;
                }
                else
                {
                    $valid['is_dashborad']= true;   
                }
                return Base::touser($valid, true);
            }
            else
            {
                return Base::touser("Invalid username or password");
            }
        }
        else if($sub_caterer)
        {
            if ((decrypt($sub_caterer->cat_usr_password) == $input['password'])) {
                        
                if(isset($input['is_mob_login']))
                {
                    $key = Base::token($sub_caterer['catt_id'], CattUserModel::class,$input['is_mob_login'],true,$sub_caterer['cat_usr_id']);    
                }
                else
                {
                    $key = Base::token($sub_caterer['catt_id'], CattUserModel::class,false,true,$sub_caterer['cat_usr_id']);   
                }
                
                $sub_caterer['token']= $key;
                $sub_caterer['caterer_admin'] =false;
                
                return Base::touser($sub_caterer, true);

        }
        else
        {
             return Base::touser("Invalid username or password");
        }
        }
        else 
        {

           return Base::touser("Account Doesn't Exists");
        }        
    }



       public function Caterer_deactivate(Request $request)
    { 

        $data = $request->input('data');
        
        $caterer                        = new CatterModel();
        $caterer                        = $caterer->where('catt_id', '=', $data['catt_id'])->first();
        $caterer->audit_deactive         = $data['status'];
        $caterer->save();
        if($caterer)
        {
            return Base::touser('Audit Status Updated', true);
        }
        else
        {   
                return Base::touser('Fail to Update');
        }

    }

    public function GetCatter_details(Request $request)
    {
        
        $token = $request->input('token');

       
        $valid = Auth_token::where('jwt_token', '=', $token)->orderBy('auth_id', 'DESC')
               ->get()->toArray();
        if($valid)
        {
                $catt_id = $valid[0]['catt_id'];

                $Catters = CatterModel::where('catt_id', '=', $catt_id)->with('cust')
               ->with('Audit')->with('special_category')->with('catt_Group.Group')->with('addi_user')->first();

                return Base::touser($Catters, true);
            
        }
        else
        {
           return Base::touser("Account Doesn't Exists");
        }        
    }

    


 public function caterer_profilepic(Request $request)
    {

         if(!empty($request->file('profile_pic')))
        {

        $image = $request->file('profile_pic');

        $input['profile_pic'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('images/caterer/profile_pic');
        $input['file_path'] = "images/caterer/profile_pic/";
        $image->move($destinationPath, $input['profile_pic']);
      
        }


       return Base::touser($input, true);

    }


    public function catgst_image(Request $request)
    {

         if(!empty($request->file('gstpic')))
        {

        $image = $request->file('gstpic');

        $input['gstpic'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('images/caterer/gstimage');
        $input['file_path'] = "images/caterer/gstimage/";
        $image->move($destinationPath, $input['gstpic']);
      
        }


       return Base::touser($input, true);

    }

     public function catfood_image(Request $request)
    {

         if(!empty($request->file('foodpic')))
        {

        $image = $request->file('foodpic');

        $input['foodpic'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('images/caterer/foodimage');
        $input['file_path'] = "images/caterer/foodimage/";
        $image->move($destinationPath, $input['foodpic']);
      
        }


       return Base::touser($input, true);

    }

    public function catfile_image(Request $request)
    {

         if(!empty($request->file('filepic')))
        {

        $image = $request->file('filepic');

        $input['filepic'] = time().'.'.$image->getClientOriginalExtension();

        $input['filesize']             = $image->getClientSize();

        $destinationPath = public_path('images/caterer/fileimage');
        $input['file_path'] = "images/caterer/fileimage/";
        $image->move($destinationPath, $input['filepic']);
      
        }

       return Base::touser($input, true);

    }
       


    public function update_package(Request $request)
    {
        
        $beg_date = '2018-06-01';
        $end_date = '2018-06-24';
        // echo $dt;
        $userPackage = UserPackage::where([['beg_date','<=',$beg_date]])->get();

          foreach ($userPackage as $use => $user) {
         
          $users_val=User::where([['user_id','=',$user->user_id],['role_id','=','2']])->first()->toArray();
             if($users_val['user_id'])
            {   
            $email = $users_val['email'];
            $userPackage = UserPackage::find($user->id);
            $userPackage->end_date=$end_date;
            $userPackage->save();

            // $user = \App\Models\User::find(46158);
            try{
            $user = \App\Models\User::find($user->user_id);

            if($user->user_id)
            {            
                $notification =  $user->notify(new \App\Notifications\Packageupdate($user));
            event(new \App\Events\NotificationEvent($user));
            }
            }
            catch (\Exception $e) {
                echo $user->user_id.'-';
                Mail::raw($userPackage, function ($message){
            $message->to('abinayah@way2smile.com');
            });
            }

      } 
       Mail::raw($userPackage, function ($message){
            $message->to('abinayah@way2smile.com');
 });
        // Mail::raw($userPackage, function($message) use ($userPackage) {

        //         $message->to('abinayah@way2smile.com',"Abi")
        //         ->from('bd@manageteamz.com', 'fromName')
        //         ->replyTo('bd@manageteamz.com', 'fromName')
        //         ->subject("Package Upgrade!");
        //     });


        return Base::touser($userPackage, true);
    }
}

    public function index(Request $request)
    {

        
        $data = CatterModel::where('is_active', true)->with('cust')
                    ->get()->toArray();
        
        return Base::touser($data, true);
    }

    public function forgetpassword(Request $request)
    {

        $rules = [
            'email' => 'required|email'            
        ];

        $data = $request->input('data');

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $caterer = CatterModel::where('catt_email_address', $data['email'])->first();
        if(!$caterer)
        {
                return Base::touser("Account Doesn't Exists");
        }
        else
        {
        
        if (!$caterer->is_active) {
            return Base::touser("Your Account is not active");
        }

        }


       $new =  str_random(6);

       $caterer->catt_password = encrypt($new);
       $caterer->is_reseted_password = true;
       $caterer->save();

       self::send_mail($data['email'],$new);

        // \App\Http\Controllers\NotificationsController::resetPassword($user);

        return Base::touser("New password has been sent to your e-mail address ", true);

    }

    public static function Request_audit (Request $request)
    {

        $data = $request->input('token');

        $catt_id = Base::Token_fromID($data);   

        $caterer = CatterModel::where('catt_id', $catt_id)->first();

        $caterer->content = <<<HTML
    <html>
    <body><h3>Hi, Admin <br/> You have request to Audit from $caterer->catt_first_name </h3>
    <p>Caterer Details are Below:</p>
    <p>Email:$caterer->catt_email_address</p>
    <p>Mobile:$caterer->catt_mobile_no</p>
    </body>
    </html>
HTML;
        // print($data);
            Mail::send([], [], function($message) use ($caterer) {
            $message->from('no-reply@platoss.com','noreply');
            $message->to('admin@platos.in','Admin');
            $message->subject("Requesting Audit from Caterer");
            $message->setBody($caterer->content, 'text/html');
        });

       return response()->json(['data' => 'Mail Sent','status' => 'ok']); 


    }
    public static function send_mail($mail,$pass)
    {
        
        $caterer = CatterModel::where('catt_email_address', $mail)->first();

        $caterer->content = <<<HTML
    <html>
    <body><h3>Hi,$caterer->catt_first_name</h3>
    <p>Your are Requested to reset password.</p>
    <p>Email:$caterer->catt_email_address</p>
    <p>New Password:$pass</p>
    </body>
    </html>
HTML;
        // print($data);
            Mail::send([], [], function($message) use ($caterer) {
            $message->from('gokulnath@way2smile.com','Admin');
            $message->to($caterer->catt_email_address);
            $message->subject("Reset Password from Platos");
            $message->setBody($caterer->content, 'text/html');
        });

       return response()->json(['data' => 'Mail Sent','status' => 'ok']); 
    }


    public function resetpassword(Request $request)
    {
        $rules = [
            'new_password'     => 'required',
            'confirm_password' => 'required|same:new_password',
            'token'          => 'required',
        ];

        $data = $request->input('data');


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        if($data['is_admin']==true)
        {
            $catt_id = Base::Token_fromID($data['token']);    
            
                $Catters = CatterModel::where('catt_id',$catt_id)->first();
                if($Catters)
                {   

                    $Catters->catt_password = encrypt($data['new_password']);
                    $Catters->is_reseted_password = false;
                    $Catters->save();


                } else {
                    return Base::throwerror();
                }

        }
        else
        {

             $sub_caterer_id = Base::Tokenfrom_SubcaterID($data['token']);    
            
                $Catters = CattUserModel::where('cat_usr_id',$sub_caterer_id)->first();
                if($Catters)
                {   
                    $Catters->cat_usr_password = encrypt($data['new_password']);
                    $Catters->save();

                } else {
                    return Base::throwerror();
                }

        }
          
            return Base::touser('Password Changed', true);      
    }

    public function resetpasswordbyemp(Request $request)
    {
        $rules = [
            'new_password'     => 'required',
            'confirm_password' => 'required|same:new_password',
        ];

        $data = $request->input('data');

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        try {
            $reset           = User::find($this->emp_id);
            $reset->user_pwd = encrypt($data['new_password']);
            $reset->save();
            return Base::touser('Password Changed', true);
        } catch (\Exception $e) {
            return Base::throwerror();
        }
    }

    public static function roles()
    {
        return Base::touser(UserRole::all(), true);
    }

    public static function managers()
    {
        try {
            $manager = UserRole::where('name', Base::manager())->get()->toArray();

            $role = $manager[0]['role_id'];

            return Base::touser(User::where('role_id', $role)->get(), true);
        } catch (\Exception $e) {
            return Base::throwerror();
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'catt_first_name'    => 'required',
            'catt_email_address' => 'required',
            'catt_mobile_no'  => 'required',
            'catt_password'      => 'required',
            'catt_address'        => 'required',
            'catt_lead_time'          => 'required',
            'DeliveryTime_from'         => 'required',
            'DeliveryTime_to'       => 'required',
            'catt_max_order'       => 'required',
            "catt_min_order"=>'required'            
            // 'email'      => 'required|email|unique:user',
        ];

        $data = $request->input('data');
        $image= $request->input('image');
        // print_r($image);exit;
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

       $totalcount = CatterModel::where('catt_email_address', $data['catt_email_address'])->count();

       
        
        if($totalcount==0)
        {

        $caterer                        = new CatterModel();
        $caterer->catt_first_name       = $data['catt_first_name'];
        $caterer->catt_sur_name         = $data['catt_sur_name'];
        $caterer->catt_email_address    = $data['catt_email_address'];
        $caterer->catt_mobile_no        = $data['catt_mobile_no'];
        $caterer->catt_password         = encrypt($data['catt_password']);
        // $caterer->catt_delivery_pincode = $data['catt_delivery_pincode'];
        $caterer->catt_delivery_fromtime= $data['DeliveryTime_from'];
        $caterer->catt_delivery_totime  = $data['DeliveryTime_to'];
        $caterer->catt_min_order        = $data["catt_min_order"];
        $caterer->catt_max_order        = $data['catt_max_order'];
        $caterer->catt_lead_time        = $data['catt_lead_time'];

        $caterer->catt_score            = isset($data['catt_score'])? $data['catt_score']: "";
        $caterer->catt_event_type       = isset($data['catt_event_type'])? $data['catt_event_type'] : "";

        $caterer->catt_gst_certificate  = isset($data['GST_image'])?$data['GST_image']:"";
        $caterer->catt_food_certificate =  isset($data['Food_image'])?$data['Food_image']:"";
        
        $caterer->catt_profile_pic  =  isset($data['catt_profile_pic'])?$data['catt_profile_pic']:"";
        
        $caterer->save();
        
        $Catter_id = $caterer->catt_id;
        
        $n = count($data['catt_address']);

        for ($i=0; $i <= $n - 1; $i++) { 
            
            $caterer_add                        = new CattAddressModel();
            $caterer_add->catt_id               = $Catter_id;
            $caterer_add->catt_city             = $data['catt_address'][$i]['address'];
            $caterer_add->catt_pincode          = $data['catt_address'][$i]['pincode'];
            $caterer_add->save();
           // echo $i;
        }

            $audit                        = new CatererAuditModel();
            $audit->catt_id               = $Catter_id;
            $audit->audit_file            = isset($data['Audit_image'])?$data['Audit_image']:""; 

            $audit->file_size               =isset($data['audit_filesize'])?$data['audit_filesize']:""; 
            $audit->save();

        return Base::touser('Caterer Created', true);

        }
        else
        {
            return Base::touser('Caterer Already Exists');
        }
          
    }

    public function show(Request $request,$id)
    {
        
            $data = CatterModel::where('catt_id',$id)->with('cust')->with('Audit')->with('addi_user')->get()->toArray();

              if($data)
              {
                    return Base::touser($data, true);
              }
              else {
                return Base::throwerror();
            }
     }


    
    public function update(Request $request, $id)
    {
        $data = $request->input('data');


        // print_r($data);exit;
        if(isset($data['from_admin']))
        {
            $catt_id = $id;
        }
        else
        {
            $catt_id = Base::Token_fromID($id);    
        }
        

        $caterer                        = new CatterModel();
        $caterer                        = $caterer->where('catt_id', '=', $data['catt_id'])->first();
        $caterer->catt_first_name       = isset($data['catt_first_name'])?$data['catt_first_name']:"";
        $caterer->catt_sur_name         = isset($data['catt_sur_name'])?$data['catt_sur_name']:"";
        $caterer->catt_email_address    = isset($data['catt_email_address'])?$data['catt_email_address']:"";
        $caterer->catt_mobile_no        = isset($data['catt_mobile_no'])?$data['catt_mobile_no']:"";
        
        
        $caterer->catt_delivery_fromtime= isset($data['DeliveryTime_from'])?$data['DeliveryTime_from']:"";
        $caterer->catt_delivery_totime  = isset($data['DeliveryTime_to'])?$data['DeliveryTime_to']:"";
        $caterer->catt_min_order        = isset($data["catt_min_order"])?$data['catt_min_order']:"";
        $caterer->catt_max_order        = isset($data['catt_max_order'])?$data['catt_max_order']:"";
        $caterer->catt_lead_time        = isset($data['catt_lead_time'])?$data['catt_lead_time']:"";

        $caterer->catt_score            = isset($data['catt_score'])? $data['catt_score']: "";
        $caterer->catt_event_type       = isset($data['catt_event_type'])? $data['catt_event_type'] : "";


         $caterer->audit_deactive       = isset($data['audit_deactive'])? $data['audit_deactive'] : "";


        if(isset($data['catt_gst_certificate']))
        {
            $caterer->catt_gst_certificate  = $data['catt_gst_certificate'];    
        }
        if(isset($data['catt_food_certificate']))
        {
        $caterer->catt_food_certificate =  $data['catt_food_certificate'];
        }

        if(isset($data['catt_profile_pic']))
        {
        $caterer->catt_profile_pic =  $data['catt_profile_pic'];
        }
        $caterer->save();
        
        $Catter_id = $caterer->catt_id;
        
        $n = count($data['catt_address']);
        if($n>0)
        {
        for ($i=0; $i <= $n - 1; $i++) { 
            
            $caterer_add                        = new CattAddressModel();
            $caterer_add->catt_id               = $Catter_id;
            $caterer_add                        = $caterer_add->where('cat_add_id', '=', $data['catt_address'][$i]['cat_add_id'])->first();
            $caterer_add->catt_city             = $data['catt_address'][$i]['catt_city'];
            $caterer_add->catt_pincode          = $data['catt_address'][$i]['catt_pincode'];
            $caterer_add->save();
           // echo $i;
        }
        }

        $n = count($data['catt_add_users']);
        if($n>0)
        {
        for ($i=0; $i <= $n - 1; $i++) { 
            
            $cat_user_add                        = new CattUserModel();
            $cat_user_add->catt_id               = $Catter_id;

            if($data['catt_add_users'][$i]['addi_cat_id'])
            {
                $cat_user_add                        = $cat_user_add->where('cat_usr_id', '=', $data['catt_add_users'][$i]['addi_cat_id'])->first();
            }
            $cat_user_add->cat_usr_name          = $data['catt_add_users'][$i]['name'];
            $cat_user_add->cat_usr_email          = $data['catt_add_users'][$i]['email'];
            $cat_user_add->cat_usr_mobile          = $data['catt_add_users'][$i]['phone'];

            if(isset($data['catt_add_users'][$i]['password']))
            {
            $cat_user_add->cat_usr_password        = encrypt($data['catt_add_users'][$i]['password']);
            }

             if(isset($data['catt_add_users'][$i]['addprofile']))
            {
            $cat_user_add->cat_usr_profile_pic        = $data['catt_add_users'][$i]['addprofile'];
            }

            

            $cat_user_add->save();
           // echo $i;
        }
        }

            if(isset($data['Audit_image']))
            {

            $audit                        = new CatererAuditModel();
            $audit->catt_id               = $Catter_id;
            $audit->audit_file            = isset($data['Audit_image'])?$data['Audit_image']:""; 
            $audit->save();
        }

        return Base::touser('Caterer Updated', true);

        }
        
    
    public function setcategory_menu(Request $request, $id)
    {


     $data = $request->input('data');

     $catt_id = Base::Token_fromID($id);


    $n = count($data['menu_list']);
        if($n>0)
        {

            $api = new CatterGroupModel();
            $api->where('catt_id', '=', $catt_id)->delete();            

        for ($i=0; $i <= $n - 1; $i++) { 

            $caterergrop_add                     = new CatterGroupModel();
            $caterergrop_add->catt_id            = $catt_id;
            $caterergrop_add->menugroup_id       = $data['menugroup_id'];
            $caterergrop_add->menu_id            = $data['menu_list'][$i]['menu_id'];
            $caterergrop_add->save();
           // echo $i;
        }
        }

        return Base::touser('Menu Updated', true);


    }

    public function destroy($id)
    {

        try {

            $api = new CatterModel();
            $api = $api->find($id);
            $api->delete();
            return Base::touser('Caterer Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Caterer its connected to Other Data !");
            //return Base::throwerror();
        }

    }

    public function recover(Request $request)
    {
        $api  = new User();
        $id   = $request->input('id');
        $user = $api->onlyTrashed()->where('user_id', '=', $id)->first();
        $user->restore();
        return Base::touser('Employee Recovered', true);
    }
}
