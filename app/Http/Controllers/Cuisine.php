<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\CuisineModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Cuisine extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = CuisineModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = CuisineModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    

    public function store(Request $request)
    {
        // 
        $rules = [
            'cuisine_name'    => 'required'               
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $evt                      = new CuisineModel();
        $evt->cuis_name           = $data['cuisine_name'];
        $evt->save();

        return Base::touser('Cuisine Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }


    public function show(Request $request,$id)
    {
            

            $data = CuisineModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'cuisine_name'    => 'required'     
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $evt                    = new CuisineModel();
        $evt                    = $evt->where('cuis_id', '=', $id)->first();
        $evt->cuis_name         = $data['cuisine_name'];
        $evt->save();
        return Base::touser('Cuisine Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new CuisineModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Cuisine Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Cuisine its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
