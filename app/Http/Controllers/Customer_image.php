<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\CustomerImageModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Customer_image extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = CustomerImageModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = CustomerImageModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    

    public function store(Request $request)
    {
        // 
        $rules = [
            'cust_image'    => 'required'     
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $testi                    = new CustomerImageModel();
        $testi->cust_image       = $data['cust_image'];
        $testi->save();

        return Base::touser('Customer Image Created', true);
    
    }


    public function image_upload(Request $request)
    {
       
        if(!empty($request->file('pic')))
        {

        $image = $request->file('pic');

        $input['pic'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('images/customer_image/');
        $input['file_path'] = "images/customer_image/";
        $image->move($destinationPath, $input['pic']);
      
        }


       return Base::touser($input, true);
    }


    public function show(Request $request,$id)
    {
            

            $data = CustomerImageModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'cust_image'    => 'required'       
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $testi                    = new CustomerImageModel();
        $testi                    = $testi->where('custimg_id', '=', $id)->first();
        $testi->cust_image       = $data['cust_image'];  
                
        $testi->save();
        return Base::touser('Customer image Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new CustomerImageModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Customer image Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Customer image its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
