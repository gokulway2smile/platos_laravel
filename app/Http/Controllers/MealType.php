<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\MealTypeModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class MealType extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = MealTypeModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = MealTypeModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    

    public function store(Request $request)
    {
        // 
        $rules = [
            'mealtype_name'    => 'required'               
        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $evt                            = new MealTypeModel();
        $evt->mealtype_name           = $data['mealtype_name'];
        $evt->save();

        return Base::touser('Meal Type Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }


    public function show(Request $request,$id)
    {
            

            $data = MealTypeModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'mealtype_name'    => 'required'     
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $evt                    = new MealTypeModel();
        $evt                    = $evt->where('mealtype_id', '=', $id)->first();
        $evt->mealtype_name         = $data['mealtype_name'];
        $evt->save();
        return Base::touser('Meal Type Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new MealTypeModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Meal Type Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to Meal Type Group its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
