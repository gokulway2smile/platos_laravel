<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;

use App\Http\Controllers\restrictcontroller;

use App\Models\CatterModel;
use App\Models\MenuModel;
use App\Models\MenuImageModel;
use App\Models\Auth_token;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\CuisineModel;
use App\Models\MealTypeModel;
use App\Models\PackageModel;



use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Menu extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = MenuModel::where('is_active', 1)->get()->orderBy('menu_name', 'asc')->toArray();

            } else {
                $data = MenuModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    


     public function image_upload(Request $request)
    {
       
       if ($request->hasFile('pic')) {
            $files = $request->file('pic');
            foreach($files as $file){
                $extension = $file->getClientOriginalExtension();
                $fileName = time().'.'.$extension;
                $folderpath  = public_path('images/menu_images/');
                $file->move($folderpath , $fileName);
                $file_name[]=$fileName;
            }
            return Base::touser($file_name, true);
            }              
    }



    public function importExcel(Request $request)
{
    if($request->hasFile('import_file')){

        $catt_id = $request->input('catt_id');
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path, function($reader) {})->get();

        if(!empty($data) && $data->count()){

            foreach ($data->toArray() as $key => $value) {

                if(!empty($value)){
    

        $cuis_id = CuisineModel::select('cuis_id')->where('cuis_name','=',$value['cuisine_type_id'])->get()->first();

        $pack_id = PackageModel::select('pack_id')->where('pack_name','=',$value['package_type_id'])->get()->first();
        $meal_type_id = MealTypeModel::select('mealtype_id')->where('mealtype_name','=',$value['meal_type_id'])->get()->first();
        
        $insert[] = ['catt_id'=>$catt_id,'menu_name' => $value['menu_name'], 'menu_description' => $value['menu_description'],'min_order' => $value['min_order'],'max_order' => $value['max_order'],'is_veg' => $value['is_veg'],'meal_type_id' => $meal_type_id['mealtype_id'],'cuisine_type_id' => $cuis_id['cuis_id'],'package_type_id' => $pack_id['pack_id'],'per_person_cost' => $value['per_person_cost'],'is_recommended' => $value['is_recommended'],'discount_off' => $value['discount_off'],'discount_duration' => $value['discount_duration'],'created_at'=>now(),'updated_at'=>now()];

                    // }
                }
            }

            if(!empty($insert)){
                
                $ipt = MenuModel::insert($insert);
                return Base::touser('Insert Record successfully', true);
                 
            }
        }
    }

    return Base::touser('Please Check your file, Something is wrong there');    

}


    public function store(Request $request)
    {
        // 
        $rules = [
            'menu_name'    => 'required',               
            'menu_description'    => 'required',
            // 'min_order'    => 'required',
            // 'max_order'    => 'required',
            'is_veg'    => 'required',
            'meal_type_id'    => 'required',
            'cuisine_type_id'    => 'required',
            'package_type_id'    => 'required',
            // 'group_id'    => 'required',
            'catt_id'       =>'required'


        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        // menu added caterer is_menu_added

        $caterer                        = new CatterModel();
        $caterer                        = $caterer->where('catt_id', '=', $data['catt_id'])->first();
        $caterer->is_menu_added         = 1;
        $caterer->save();

        $Menu                      = new MenuModel();
        $Menu->menu_name           = $data['menu_name'];
        $Menu->catt_id             = $data['catt_id'];
        $Menu->menu_description    = $data['menu_description'];
        // $Menu->min_order           = $data['min_order'];
        // $Menu->max_order           = $data['max_order'];
        $Menu->is_veg              = $data['is_veg'];
        $Menu->meal_type_id        = $data['meal_type_id'];
        $Menu->cuisine_type_id     = $data['cuisine_type_id'];
        $Menu->package_type_id     = $data['package_type_id'];
        // $Menu->group_id            = $data['group_id'];
        $Menu->per_person_cost     = $data['per_person_cost'];
        $Menu->is_recommended      = $data['is_recommended'];

        $Menu->discount_off        = $data['discount'];
        $Menu->discount_duration   = $data['discount_duration'];

        $Menu->save();

        $menulast_id = $Menu['menu_id'];
        foreach ($data['menu_images'][0] as $key => $value) {
        
        $menu_image                      = new MenuImageModel();
        $menu_image->menu_id             = $menulast_id;
        $menu_image->menu_imgname        = $value;
        
        $menu_image->save();

        
       }

        return Base::touser('Menu Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }



    public function Menu_Live(Request $request)
    {
         // menu added caterer is_menu_added

        $data = $request->input('token');

        $catt_id = Base::Token_fromID($data);  

        $caterer                        = new CatterModel();
        $caterer                        = $caterer->where('catt_id', '=', $catt_id)->first();
        $caterer->is_menu_added         = 2;
        $caterer->save();

    }
    public function show(Request $request,$id)
    {
            
            $data = MenuModel::where('catt_id','=',$id)->with('Cuisint_type')->with('Meal_type')->with('Package_type')->with('Images')->with('Caterer')->orderBy('menu_name', 'asc')->get()->toArray();          
            
            return Base::touser($data, true);

          
    }


    public function GetMenufrom_Admin(Request $request,$id)
    {
            
            $data = MenuModel::where('catt_id','=',$id)->with('Cuisint_type')->with('Meal_type')->with('Package_type')->with('Images')->with('Caterer')->orderBy('menu_name', 'asc')->get()->toArray();          
            
            return Base::touser($data, true);

          
    }
    

    

    public function GetAll_catterMenu(Request $request)
    {
            
            $token = $request->input('token');

            $catt_id= Base::Token_fromID($token);
            

            // $catt_id = $catter;
             
            // exit;

            $data = MenuModel::where('catt_id','=',$catt_id)->with('Cuisint_type')->with('Meal_type')->with('Package_type')->with('Images')->with('Caterer')->orderBy('menu_name', 'asc')->get()->toArray();          
            
            return Base::touser($data, true);
          
    }




    public function EditMenu(Request $request,$id)
    {
            

            $data = MenuModel::where('menu_id','=',$id)->with('Cuisint_type')->with('Meal_type')->with('Package_type')->with('Images')->with('Caterer')->get()->first();          
            
            return Base::touser($data, true);

          
    }

    



     public function update(Request $request, $id)
    {

        $rules = [
            'menu_name'    => 'required',               
            'menu_description'    => 'required',
            // 'min_order'    => 'required',
            // 'max_order'    => 'required',
            'is_veg'    => 'required',
            'meal_type_id'    => 'required',
            'cuisine_type_id'    => 'required',
            'package_type_id'    => 'required',
            // 'group_id'    => 'required',
            'catt_id'       =>'required'


        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }


    

        $Menu                      = new MenuModel();
        $Menu                      = $Menu->where('menu_id', '=', $id)->first();
        $Menu->menu_name           = $data['menu_name'];
        $Menu->catt_id             = $data['catt_id'];
        $Menu->menu_description    = $data['menu_description'];
        // $Menu->min_order           = $data['min_order'];
        // $Menu->max_order           = $data['max_order'];
        $Menu->is_veg              = $data['is_veg'];
        $Menu->meal_type_id        = $data['meal_type_id'];
        $Menu->cuisine_type_id     = $data['cuisine_type_id'];
        $Menu->package_type_id     = $data['package_type_id'];
        // $Menu->group_id            = $data['group_id'];
        $Menu->per_person_cost     = $data['per_person_cost'];
        $Menu->is_recommended      = $data['is_recommended'];
        $Menu->discount_off        = $data['discount'];
        $Menu->discount_duration   = $data['discount_duration'];

        $Menu->save();

        $menulast_id = $Menu['menu_id'];
        $images_count = count($data['menu_images']);
        if($images_count>0)
        {

        foreach ($data['menu_images'][0] as $key => $value) {
        
        $menu_image                      = new MenuImageModel();
        $menu_image->menu_id             = $menulast_id;
        $menu_image->menu_imgname        = $value;
        
        $menu_image->save();

        }

    }

    return Base::touser('Menu Updated', true);

}

    
      public function Menu_golive(Request $request)
    { 

        $data = $request->input('token');
        $catt_id = Base::Token_fromID($data);  

        $caterer                        = new CatterModel();
        $caterer                        = $caterer->where('catt_id', '=', $catt_id)->first();
        $caterer->is_menu_added         = 2;
        $caterer->save();
        if($caterer)
        {
            return Base::touser('Go Live Updated', true);
        }
        else
        {   
                return Base::touser('Fail to  Update');
        }

    }

    public function destroy($id)
    {

        try {

            $testi = new MenuModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Menu Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Menu its connected to Other Data !");
            //return Base::throwerror();
        }

    }

    public function deletemenu_img($id)
    {

        try {

            $testi = new MenuImageModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Menu Images Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Menu  Images its connected to Other Data !");
            //return Base::throwerror();
        }

    }



}
