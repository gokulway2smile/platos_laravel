<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\GroupMenuModel;

use App\Models\CatterGroupModel;
use App\Models\MenuModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class MenuGroup extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = GroupMenuModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = GroupMenuModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }

    public function GetCatt_Grouplist(Request $request)
    {
            $data = $request->input('token');
            $catt_id = Base::Token_fromID($data); 



         if ($data) {
                $data = GroupMenuModel::where('is_active', 1)->where('catt_id','=',$catt_id)->with('menu_group.menu')->groupBy('menugroup_id')->get();
                
            } else {
                $data = GroupMenuModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);


    }
    

    public function Get_MyGroupmenus(Request $request)
    {

        $data = $request->input('data');
        $catt_id = Base::Token_fromID($data['token']); 


         if ($data) {
                $data = CatterGroupModel::where('is_active', 1)->where('catt_id','=',$catt_id)->where('menugroup_id','=',$data['menugroup_id'])->with('Group')->with('menu')->get()->toArray();

            } else {
                $data = CatterGroupModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }

    public function store(Request $request)
    {
        // 
        $rules = [
            'group_name'    => 'required'               
        ];

        $data = $request->input('data');
        //return $data["comments1"];


        $catt_id = Base::Token_fromID($data['token']); 

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $evt                            = new GroupMenuModel();
        $evt->catt_id                   = $catt_id;
        $evt->menugroup_name            = $data['group_name'];
        $evt->save();

        return Base::touser('Group Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }


    public function show(Request $request,$id)
    {
            

            $data = GroupMenuModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'group_name'    => 'required'     
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $catt_id = Base::Token_fromID($id); 

        $evt                    = new GroupMenuModel();
        $evt                    = $evt->where('menugroup_id', '=', $id)->first();
        $evt->catt_id           = $catt_id;
        $evt->menugroup_name    = $data['group_name'];
        $evt->save();
        return Base::touser('Group Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new GroupMenuModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Group Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Group its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
