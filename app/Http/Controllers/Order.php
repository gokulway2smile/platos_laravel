<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\OrderModel;
use App\Models\OrderListModel;
use App\Models\UserModel;
use App\Models\MenuModel;
use App\Models\MenuImageModel;

use Carbon\Carbon;
use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Order extends Controller
{
    
    public function index(Request $request)
    {
        $id = $request->input('id');
        
        
        $data = OrderModel::where('ord_catter_id','=',$id)->orderBy('created_at', 'desc')->get()->toArray();

          return Base::touser($data, true);

    }
    
    public function getOrderlist_admin(Request $request)
    {
        $data = OrderModel::where('is_active','1')->orderBy('created_at', 'desc')->with('Userslist')->with('OrderList.menu_details.Images')->get()->toArray();

          return Base::touser($data, true);
    }


    public function store(Request $request)
    {
        $rules = [
            'catt_id'    => 'required'

        ];

        $data = $request->input('data');
        //return $data["comments1"];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $orders                           = new OrderModel();
        $orders->ord_user_id              = "7";
        $orders->ord_catter_id            = $data['catt_id'];
        $orders->ord_billing_adddres_id   = "1";
        $orders->ord_shipping_addres_id   = "1";
        $orders->ord_price_amount         = $data['amount'];
        $orders->ord_total_amount         = $data['amount'];;
        $orders->ord_status               = "Order_placed";
        $orders->save();

        $last_orderId = $orders->ord_id;

        $orders_list                           = new OrderListModel();
        $orders_list->ord_list_order_id        = $last_orderId;
        $orders_list->menu_id                  = $data['menu_id'];
        $orders_list->ord_list_quantity        = $data['quantity'];
        $orders_list->save();

        return Base::touser('Orders Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }


    public function show(Request $request,$id)
    {
            
         $data = OrderModel::where('ord_catter_id','=',$id)->where('ord_status','!=','Cancelled')->where('ord_status','!=','Order_placed')->with('OrderList.menu_details.Images')->with('Userslist')->get()->toArray();

          return Base::touser($data, true);

          
    }

      public function GetAllMy_Orders(Request $request)
    {

         $token = $request->input('token');
        
         $catt_id= Base::Token_fromID($token);
         
         $orders = [];
    $Past_orders = OrderModel::where('ord_catter_id', '=', $catt_id)->where('ord_status','!=','Order_placed')->where(function($q) {
         $q->where('ord_status', 'Cancelled')
           ->orWhere('ord_status', 'Delivered');
     })->with('OrderList.menu_details.Images')->with('Userslist')->get()->toArray();

                
    $from = Carbon::now()->addWeek();   
    $to = Carbon::now()->subWeek();   
    $current = Carbon::now();

    $present_orders = OrderModel::where('ord_catter_id','=',$catt_id)->where('ord_status','!=','Order_placed')->where(function($q) {
         $q->where('ord_status', 'Confirmed')
           ->orWhere('ord_status', 'Out_to_delivery');
     })->whereBetween('order_dates',[$to, $from])->with('OrderList.menu_details.Images')->with('Userslist')->get()->toArray();



    $future = OrderModel::where('ord_catter_id','=',$catt_id)->where('ord_status','!=','Order_placed')->where(function($q) {
         $q->where('ord_status', 'Confirmed')
           ->orWhere('ord_status', 'Out_to_delivery');
     })->whereDate('order_dates','>=', $from)->with('OrderList.menu_details.Images')->with('Userslist')->get()->toArray();

        $orders['past_orders'] = $Past_orders;

        $orders['present_orders'] = $present_orders;

        $orders['future_orders'] = $future;
          

          return Base::touser($orders, true);

          
    }

    
    public function GetMyMIS_reports(Request $request)
    {

        $token = $request->input('token');
        
         $catt_id= Base::Token_fromID($token);
            
         $data = OrderModel::where('ord_catter_id','=',$catt_id)->with('OrderList.menu_details.Images')->with('Userslist')->get()->toArray();

          return Base::touser($data, true);


    }


     public function New_orders(Request $request)
    {
            
          $token = $request->input('token');
        
          $catt_id= Base::Token_fromID($token); 
         

         $data = OrderModel::where('ord_catter_id','=',$catt_id)->where('ord_status','Order_placed')->with('OrderList.menu_details.Images')->with('Userslist')->get()->toArray();

          return Base::touser($data, true);

          
    }
    


     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        
        $order                    = new OrderModel();
        $order                    = $order->where('ord_id', '=', $id)->first();
        $order->ord_status        = isset($data['ord_status'])?$data['ord_status']:"";
        $order->reason_cancel     = isset($data['reason_cancel'])?$data['reason_cancel']:"";
        

        $order->save();
        return Base::touser('Order Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new OrderModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Event Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Event its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
