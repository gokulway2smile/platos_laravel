<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base;
use App\Http\Controllers\restrictcontroller;

use App\Models\SpecialCategoryModel;

use Illuminate\Http\Request;

use Validator;
use Mail;
use Illuminate\Notifications\Messages\MailMessage;

class Specialcategory extends Controller
{
    
    public function index(Request $request)
    {
            
             if ($request->input('active')) {
                $data = SpecialCategoryModel::where('is_active', 1)->get()->toArray();

            } else {
                $data = SpecialCategoryModel::where('is_active', 1)->get()->toArray();
            }

          return Base::touser($data, true);

    }
    

    public function store(Request $request)
    {
        // 
        $rules = [
            'category_name'    => 'required'               
        ];

        $data = $request->input('data');
        //return $data["comments1"];

          

        $catt_id= Base::Token_fromID($data['token']);


        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

    

        $evt                      = new SpecialCategoryModel();
        $evt->category_name       = $data['category_name'];
        $evt->catt_id             = $catt_id;
        $evt->save();

        return Base::touser('Category Created', true);
    
    }


    // public function image_upload(Request $request)
    // {
       
    //     if(!empty($request->file('pic')))
    //     {

    //     $image = $request->file('pic');

    //     $input['pic'] = time().'.'.$image->getClientOriginalExtension();

    //     $destinationPath = public_path('images/evtmonial/');
    //     $input['file_path'] = "images/evtmonial/";
    //     $image->move($destinationPath, $input['pic']);
      
    //     }


    //    return Base::touser($input, true);
    // }


    public function show(Request $request,$id)
    {
            

            $data = SpecialCategoryModel::find($id)->toArray();          
            
            return Base::touser($data, true);

          
    }

     public function update(Request $request, $id)
    {
        $data = $request->input('data');

        $rules = [
            'category_name'    => 'required'     
        ];

        
        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return Base::touser($validator->errors()->all()[0]);
        }

        $catt_id= Base::Token_fromID($data['token']);


        $evt                    = new SpecialCategoryModel();
        $evt                    = $evt->where('category_id', '=', $id)->first();
        $evt->category_name     = $data['category_name'];
        $evt->catt_id           = $catt_id;
        $evt->save();
        return Base::touser('Category Updated', true);
    }

    public function destroy($id)
    {

        try {

            $testi = new SpecialCategoryModel();
            $testi = $testi->find($id);
            $testi->delete();
            return Base::touser('Category Deleted', true);

        } catch (\Exception $e) {

            return Base::touser("Can't able to delete Category its connected to Other Data !");
            //return Base::throwerror();
        }

    }

}
