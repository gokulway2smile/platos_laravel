<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class CattUserModel extends Model 
{
    
    // public function cust()
    // {
    //     return $this->hasMany('App\Models\CattAddressModel', 'catt_id','catt_id');
    // }



    // public function Audit()
    // {
    //     return $this->hasMany('App\Models\CatererAuditModel', 'catt_id','catt_id');
    // }

    // public function catt_Group()
    // {
    //     return $this->hasMany('App\Models\CatterGroupModel', 'catt_id','catt_id');
    // }
    


    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_caterer_users';


    protected $primaryKey = 'cat_usr_id';

    public $timestamps = true;

    protected $hidden = array('cat_usr_password');

    protected $dates = ['deleted_at'];




    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'catt_id',
        'cat_usr_mobile',
        'cat_usr_email',
        'cat_usr_mobile',
        'cat_usr_password',
        'cat_usr_profile_pic'
                  
    ];

    protected $guarded = [];
}
