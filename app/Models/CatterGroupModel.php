<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class CatterGroupModel extends Model 
{
    
    public function Group()
    {
        return $this->hasOne('App\Models\GroupMenuModel', 'menugroup_id','menugroup_id');
    }

    public function menu()
    {
        return $this->hasOne('App\Models\MenuModel', 'menu_id','menu_id');
    }


    

    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_caterer_group';


    protected $primaryKey = 'catt_group_id';

    public $timestamps = true;

    protected $dates = ['deleted_at'];



    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'catt_id',
        'menugroup_id',
        'menu_id'        
    ];

    protected $guarded = [];
}
