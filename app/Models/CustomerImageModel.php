<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class CustomerImageModel extends Model 
{
    
    // public function cust()
    // {
    //     return $this->hasMany('App\Models\Customer', 'emp_id','user_id');
    // }


    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_customer_image';


    protected $primaryKey = 'custimg_id';

    public $timestamps = true;

    protected $dates = ['deleted_at'];



    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'cust_image'        
    ];

    protected $guarded = [];
}
