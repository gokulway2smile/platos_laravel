<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class GroupMenuModel extends Model 
{
    

    public function menu_group()
    {
        return $this->hasMany('App\Models\CatterGroupModel', 'menugroup_id','menugroup_id');
    }

    

    public function catt_Group()
    {
        return $this->hasMany('App\Models\CatterGroupModel', 'catt_id','catt_id');
    }
    
    // public function cust()
    // {
    //     return $this->hasMany('App\Models\Customer', 'emp_id','user_id');
    // }


    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_menugroup';


    protected $primaryKey = 'menugroup_id';

    public $timestamps = true;

    protected $dates = ['deleted_at'];



    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'catt_id',
        'menugroup_name'        
    ];

    protected $guarded = [];
}
