<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class MenuModel extends Model 
{
    
    public function Cuisint_type()
    {
        return $this->hasOne('App\Models\CuisineModel', 'cuis_id','cuisine_type_id');
    }

     public function Meal_type()
    {
        return $this->hasOne('App\Models\MealTypeModel', 'mealtype_id','meal_type_id');
    }

     public function Package_type()
    {
        return $this->hasOne('App\Models\PackageModel', 'pack_id','package_type_id');
    }

    public function Caterer()
    {
        return $this->hasOne('App\Models\CatterModel', 'catt_id','catt_id');
    }

    public function Images()
    {
        return $this->hasMany('App\Models\MenuImageModel', 'menu_id','menu_id');
    }
    
    

    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_menu';


    protected $primaryKey = 'menu_id';

    public $timestamps = true;

    protected $dates = ['deleted_at'];



    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'menu_name',
        'menu_description',
        'min_order',
        'max_order',
        'is_veg',
        'meal_type_id',
        'cuisine_type_id',
        'package_type_id',
        'group_id',
        'per_person_cost',
        'is_recommended',
        'catt_id',
        'discount_off',
        'discount_duration'

    ];

    protected $guarded = [];
}
