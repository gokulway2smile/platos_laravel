<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class OrderListModel extends Model 
{
    
     public function menu_details()
   {
       return $this->hasOne('App\Models\MenuModel', 'menu_id', 'menu_id');
   }
   
    // public function cust()
    // {
    //     return $this->hasMany('App\Models\CattAddressModel', 'catt_id','catt_id');
    // }


    // public function role()
    // {
    //     return $this->hasOne('App\Models\MenuModel', 'menu_id', 'order_list_menu_id');
    // }

    protected $table = 'pl_order_list';


    protected $primaryKey = 'ord_list_id';

    public $timestamps = true;

    // protected $hidden = array('catt_password');

    protected $dates = ['deleted_at'];




    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'ord_list_order_id',
        'menu_id',
        'ord_list_quantity'
    ];

    protected $guarded = [];
}
