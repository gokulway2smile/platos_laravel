<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class OrderModel extends Model 
{
    
    public function OrderList()
    {
        return $this->hasMany('App\Models\OrderListModel', 'ord_list_order_id','ord_id');      

    }


    public function Userslist()
    {
        return $this->hasOne('App\Models\UserModel', 'user_id','ord_user_id');      

    }

    

    public function menu_images()
   {
       return $this->hasMany('App\Models\MenuImageModel', 'menu_id', 'menu_id');
   }



   
    protected $table = 'pl_orders';


    protected $primaryKey = 'ord_id';

    public $timestamps = true;

    // protected $hidden = array('catt_password');

    protected $dates = ['deleted_at'];




    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'ord_user_id',
        'ord_catter_id',
        'ord_billing_adddres_id',
        'ord_shipping_addres_id',
        'ord_price_amount',
        'ord_devliery_amount',
        'ord_total_amount',
        'ord_payment_mode',
        'ord_transation_status',
        'ord_transation_id',
        'ord_delivery_instruct',
        'ord_status',
        'reason_cancel',
        'order_dates'
        
                  
    ];

    protected $guarded = [];
}
