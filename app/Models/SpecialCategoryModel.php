<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use OwenIt\Auditing\Auditable;
// use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
// use Illuminate\Notifications\Notifiable;

class SpecialCategoryModel extends Model 
{
    
    // public function cust()
    // {
    //     return $this->hasMany('App\Models\Customer', 'emp_id','user_id');
    // }


    // public function role()
    // {
    //     return $this->hasOne('App\Models\UserRole', 'role_id', 'role_id');
    // }

    protected $table = 'pl_special_category_type';


    protected $primaryKey = 'category_id';

    public $timestamps = true;

    protected $dates = ['deleted_at'];



    /**
     * Display timestamps in user's timezone
     */
    protected function asDateTime($value)
    {

        $value = \App\Http\Controllers\Base::ConvertTimezone($value);

        return $value;

    }

    protected $fillable = [
        'category_name',
        'catt_id'
    ];

    protected $guarded = [];
}
