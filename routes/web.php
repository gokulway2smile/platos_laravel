<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::group(array('prefix' => 'api'), function () {


// for caterers 
Route::resource('getall_catterlist', 'Catters');

Route::post('Catter_NewAdmin', 'Catters@Create_admin');

Route::post('Caterer_Forgot', 'Catters@forgetpassword');

Route::post('catgst_image', 'Catters@catgst_image');

Route::post('catfood_image', 'Catters@catfood_image');

Route::post('catfile_image', 'Catters@catfile_image');

Route::post('GetCatter_details', 'Catters@GetCatter_details'); 

Route::post('Caterer_resetpassword', 'Catters@resetpassword'); 

Route::put('Set_MenuCategory/{id}', 'Catters@setcategory_menu'); 

Route::post('Caterer_Login', 'Catters@caterer_webLogin');

Route::post('catt_profile', 'Catters@caterer_profilepic');  

Route::post('Caterer_deactivate', 'Catters@Caterer_deactivate');   


// for orders

Route::resource('GetMyOrder_List', 'Order');  

Route::post('GetAllMy_Orders', 'Order@GetAllMy_Orders');   

Route::post('GetMyMIS_reports', 'Order@GetMyMIS_reports');    

Route::post('GetMyOrder_newList', 'Order@New_orders');  

Route::resource('getall_orders', 'Order');  

Route ::get("getOrderlist_admin", 'Order@getOrderlist_admin');


// for Menu

Route::resource('getAll_menu', 'Menu');  

Route::post('GetAll_catterMenu', 'Menu@GetAll_catterMenu');   

Route::post('Get_MyGroupmenus', 'MenuGroup@Get_MyGroupmenus');     

Route::get('getMenu_item/{id}', 'Menu@EditMenu');  

Route::get('getall_menulist/{id}', 'Menu@GetMenufrom_Admin');  

Route::delete('Deletemenu_img/{id}', 'Menu@deletemenu_img');

Route::post("bulk_importmenu",'Menu@importExcel');

Route::post('AddMenu_images', 'Menu@image_upload');

Route::post('caterer_menulive', 'Menu@Menu_Live');

Route::post('getmenu_golive', 'Menu@Menu_golive'); 



//for users

Route::get('getall_Userlist', 'Users@getall_list');

Route::get('get_Userdetails/{id}', 'Users@get_userdetails'); 



Route::resource('getall_customer', 'Customer_image');  

Route::post('cust_image', 'Customer_image@image_upload');

// for Admin

Route::post('web_login', 'Admin@Auth_weblogin');

// for Managements

Route::resource('getmy_specialcategory', 'Specialcategory');  

Route::resource('getall_package', 'Package');  

Route::resource('getall_mealtypes', 'MealType');  

Route::resource('getall_cuisine', 'Cuisine');  

Route::resource('getall_groups', 'MenuGroup');  

Route::resource('getall_amenities', 'Amenities');  

Route::post('getcat_grouplist', 'MenuGroup@GetCatt_Grouplist');




// Testimonial 

Route::resource('getall_testimonial', 'Testimonial'); 

Route::post('testi_image', 'Testimonial@image_upload');



//audit

Route::post('Requesting_audit', 'Catters@Request_audit');  


});



